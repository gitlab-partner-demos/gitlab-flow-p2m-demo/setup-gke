# Terraform for Kubernetes Cluster on Google Cloud

## GitOps Demo Group

```
├── backend.tf         # State file Location Configuration
├── gke.tf             # Google GKE Configuration
├── provider.tf        # Adding kubernetes details to the `helm` provider
└── gitlab-agent.tf    # Registering kubernetes agent cluster to the `Cluster Management` project
```

### TODO

- [ ] Add link to Handbook page
- [ ] Replace hardcoded `europe-west1-d`-region to use `GOOGLE_REGION` from CI/CD vars