data "google_client_config" "current" {}
data "gitlab_group" "gitops-demo-apps" {
  full_path = "63216123"
}

data "gitlab_projects" "cluster-management-search" {
  # Returns a list of matching projects. limit to 1 result matching "cluster-management"
  group_id            = data.gitlab_group.gitops-demo-apps.id
  simple              = true
  search              = "setup-gke"
  per_page            = 1
  max_queryable_pages = 1
}

resource "gitlab_cluster_agent" "gitops-agent" {
  project = data.gitlab_projects.cluster-management-search.projects.0.id
  name    = "gitops-agent"
}

resource "gitlab_repository_file" "gitops_agent_config" {
  project        = gitlab_cluster_agent.gitops-agent.project
  branch         = "main" // or use the `default_branch` attribute from a project data source / resource
  file_path      = ".gitlab/agents/${gitlab_cluster_agent.gitops-agent.name}/config.yaml"
  content        = <<CONTENT
ci_access:
    groups:
        - id: gitlab-partner-demos/gitlab-flow-p2m-demo/setup-gke
  CONTENT
  author_email   = "kristof@gitlab.com"
  author_name    = "Kristof"
  commit_message = "[skip-ci] feature: add agent config for ${gitlab_cluster_agent.gitops-agent.name}"
}

// Create token for an agent
resource "gitlab_cluster_agent_token" "example" {
  project     = gitlab_cluster_agent.gitops-agent.project
  agent_id    = gitlab_cluster_agent.gitops-agent.agent_id
  name        = "awesome-token"
  description = "awesome token"
}

// The following example creates a GitLab Agent for Kubernetes in a given project,
// creates a token and install the `gitlab-agent` Helm Chart.
// (see https://gitlab.com/gitlab-org/charts/gitlab-agent)
# data "gitlab_project" "this" {
#   path_with_namespace = "my-org/example"
# }

# resource "gitlab_cluster_agent" "this" {
#   project = data.gitlab_project.this
#   name    = "my-agent"
# }

resource "gitlab_cluster_agent_token" "this" {
  project     = gitlab_cluster_agent.gitops-agent.project
  agent_id    = gitlab_cluster_agent.gitops-agent.agent_id
  name        = "my-agent-token"
  description = "Token for the my-agent used with `gitlab-agent` Helm Chart"
}

resource "helm_release" "gitlab_agent" {
  depends_on = [
    google_container_cluster.primary
  ]
  name             = "gitlab-agent"
  namespace        = "gitlab-agent"
  create_namespace = true
  repository       = "https://charts.gitlab.io"
  chart            = "gitlab-agent"
  //version          = "1.2.0"

  set {
    name  = "config.token"
    value = gitlab_cluster_agent_token.this.token
  }
}
