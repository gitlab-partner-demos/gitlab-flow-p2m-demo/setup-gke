variable "google_cloud_region" {
  description = "Target deployment region"
  type        = string
  default     = "us-central1"
}